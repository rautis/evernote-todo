#!/usr/bin/env ruby
# encoding: iso-8859-1
require 'trollop'
require 'evernote'
require 'iconv'

require File.expand_path('evernote_access')

class EvernoteCLI

	def initialize()
		@ea = EvernoteAccess.new		
	end
	
	def fix_text_encoding(note_text)
		nt = note_text.dup		
		nt = nt.force_encoding("iso-8859-1") .encode("utf-8")
		return nt
	end

	def add_note(opts)
		@ea.addNote(opts[:notebook], fix_text_encoding(opts[:title]), fix_text_encoding(opts[:note]), opts[:tag])
	end

	def add_empty_note(opts)
		@ea.addNote(opts[:notebook], opts[:title], "", opts[:tag])
	end

	def add_note_with_default_title(opts)		
		note = fix_text_encoding(opts[:note])
		slen = (note.length > 30) ? 30 : note.length
		title = note[0..slen]
		@ea.addNote(opts[:notebook], title, note, opts[:tag])
	end

	def list_notes(opts)
		@ea.formatNotes(@ea.getNotes(opts[:notebook], opts[:tag]))
	end

	def modify_note(opts)
		@ea.updateNote(opts[:guid], opts[:title], opts[:note], opts[:tag])
	end

	def print_data(nb, tags)
		@ea.formatNotes(@ea.getNotes(nb, tags))
		puts ""
		@ea.formatTags()
		puts ""
		print "Commands:\nd note_id = delete note\nu note_id new_tags\nq = Quit\n"
		print "cmd:>"
	end

	def modify_note_interactive(opts)
		print_data(opts[:notebook], opts[:tag])
		while cmd = STDIN.gets
			carr = cmd.split
			opc = carr.shift
			case opc
				when 'q'
					break
				when 'd'
					id = carr.shift
					@ea.deleteNote(@ea.note_id_map[id.to_i])
				when 'u'
					id = carr.shift
					@ea.updateNote(@ea.getGuidForIndex(id.to_i), nil, nil, carr)
				else
					puts "Unknown command"
			end
			print_data(opts[:notebook], opts[:tag])
		end
	end
end

if __FILE__ == $PROGRAM_NAME
	opts = Trollop::options do  
		banner("Add note to Evernote")
		opt :add, 'Add new note to notebook'
		opt :list, 'List notes'
		opt :modify, 'Modify note tags / note'
		opt :notebook, 'Notebook name', :type => String, :required => true
		opt :title, 'Note title', :type => String
		opt :note, 'Note text (use quotation)', :type => String
		opt :tag, 'Tag for the note', :type => String, :multi => true
		opt :guid, 'GUID of the note (needed in modifications). Use list command to find out it', :type => String
	end

	begin
		ec = EvernoteCLI.new
		ec.add_note(opts) if (opts[:add_given] && opts[:title_given] && opts[:note_given])
		ec.add_empty_note(opts) if (opts[:add_given] && opts[:title_given] && !opts[:note_given])
		ec.add_note_with_default_title(opts) if (opts[:add_given] && !opts[:title_given] && opts[:note_given])
		ec.list_notes(opts) if (opts[:list_given] && !opts[:tag_given])
		ec.list_notes(opts) if (opts[:list_given] && opts[:tag_given])
		ec.modify_note(opts) if (opts[:modify_given] && opts[:guid_given])
		ec.modify_note_interactive(opts) if (opts[:modify_given] && !opts[:guid_given])
	rescue Evernote::EDAM::Error::EDAMUserException => e
			p "Evernote error"
			p e.errorCode
			p e.parameter
	rescue Evernote::EDAM::Error::EDAMSystemException => e
			p "Evernote error"
			p e.errorCode
			p e.message	
	end
end