require 'rubygems'
require 'evernote'

class EvernoteAccess

	attr_accessor :user, :auth_token, :note_id_map
	attr_accessor :note_store, :notebooks, :tags
	
	def initialize()
		
		config = {
			:username => 'your_username',
			:password => 'your_password',
			:consumer_key => 'your_username',
			:consumer_secret => 'your_secret'
		}

		user_store_url = "https://sandbox.evernote.com/edam/user"
		user_store = Evernote::UserStore.new(user_store_url, config)
		auth_result = user_store.authenticate
		@user = auth_result.user
		@auth_token = auth_result.authenticationToken
		note_store_url = "https://sandbox.evernote.com/edam/note/#{user.shardId}"
		@note_store = Evernote::NoteStore.new(note_store_url)
		@notebooks = note_store.listNotebooks(auth_token)
		@tags = @note_store.listTags(@auth_token)
		@note_id_map = {}
	end
	
	def getDefaultNotebook()
		return @notebooks[0]
	end
	
	def getNotebook(notebook_name)
		return notebooks.select { |notebook| notebook.name == notebook_name}.first
	end
	
	def getGuidForIndex(i)
		return @note_id_map[i]
	end
	
	def addNote(notebook, note_title, note_text, tags)
		nb = getNotebook(notebook)
		note = Evernote::EDAM::Type::Note.new()
		note.title = note_title.force_encoding('utf-8')
		note.notebookGuid = nb.guid
		note.content = str2enml(note_text).force_encoding("utf-8")
		tg = createTagGuidList(getTags(tags))
		note.tagGuids = tg
		
		createdNote = @note_store.createNote(@auth_token, note)
		
		return createdNote
	end
	
	def getTags(note_tags)
		t = []
		
		@tags.each do |tag|
			note_tags.each do |ntag|
				if tag.name == ntag
					t.push(tag)
				end
			end
		end
		
		return t 
	end
	
	def getNotes(notebook, tags)
	    nb = getNotebook(notebook)
		note_filter = Evernote::EDAM::NoteStore::NoteFilter.new()
		
		if tags != nil
			if tags.size > 0
				tg = createTagGuidList(getTags(tags))
				note_filter.tagGuids = tg
			end 
		end
		
		note_filter.notebookGuid = nb.guid
		nc = @note_store.findNoteCounts(@auth_token, note_filter, false)
		
		mn = nc.notebookCounts[nb.guid]

		note_list = @note_store.findNotes(@auth_token, note_filter, 0, mn);
		
		return note_list.notes
	end
	
	def updateNote(guid, title, note, tags)
		
		note_data = @note_store.getNote(@auth_token, guid, true, false, false, false)
		
		if tags != nil 
			if tags.size > 0
				note_data.tagGuids = createTagGuidList(getTags(tags))
			end
		end
		
		if title != nil
			note_data.title = title
		end
		
		if note != nil
			note_data.content = str2enml(note)
		end
		
		return @note_store.updateNote(@auth_token, note_data)
	end
		
	def updateNoteTags(note, tags)
		tg = createTagGuidList(getTags(tags))
		note.tagGuids = tg
		return @note_store.updateNote(@auth_token, note)
	end
	
	def deleteNote(guid)
		@note_store.deleteNote(@auth_token, guid)
	end
	
	def createTagGuidList(tag_list)
		tg = []
		
		tag_list.each { |t| tg.push(t.guid)}		
		return tg
	end
	
	def str2enml(str)
		return '<?xml version="1.0" encoding="UTF-8"?>' +
			   '<!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">' +
			   '<en-note>' +
                str.force_encoding("utf-8") +
               '</en-note>'
	end
	
	def formatNotes(notes)
		puts "%-5s %-30s %s" % ["Index", "Note title", "GUID"]
		puts "=" * 70
		n = 0
		notes.each do |note|
			nstr = "[#{n}]"
			puts "%-5s %-30s %s" % [nstr, note.title, note.guid]
			@note_id_map[n] = note.guid
			n = n + 1
		end		
	end
	
	def formatTags()
		print "Available tags: ["
		@tags.each_index do |i|
			if i == 0
				print @tags[i].name
			else
				print ", " + @tags[i].name
			end
		end
		print "]\n"
	end
end

